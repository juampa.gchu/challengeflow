const express = require('express');
const request = require('request');
const ipapi = require('ipapi.co');

// Parametros generales
const port = 3000;

// Parametros API
const tokenAPI = 'fe803142ac0539e061cf5f5242ce58b5';
const hostAPI = 'https://api.openweathermap.org/data/2.5/';

var geoData = null;

//ARRANCO EXPRESS
const app = express();
// Consulto los datos geograficos del servidor
ipapi.location(function(dataIp){
	geoData = {
		city:dataIp['city'],
		lat:dataIp['latitude'],
	    lon:dataIp['longitude']
	};
	app.listen(port, startApp);
});


// Cuando finaliza todo indico las rutas en la que voy a trabajar.
function startApp(){
	console.log('SERVER OK!');
	app.get('/v1/location', getLocation);
	app.get('/v1/current/:city?', getCurrent);
	app.get('/v1/forecast/:city?', getForecast);
};

function response(data,status,res){
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
	if(typeof data  == 'string'){
		data = {'data':data};
	}
	res.status((status? status:200)).send(JSON.stringify(data));
};

function getLocation(req, res){
	response(geoData,200,res);
};

function getCurrent(req, res){
	let city = geoData['city'];
	if(req['params'] && req['params']['city']){
		city = req['params']['city'];
	}
	getDataNowByNameCity(city,function(resp){
		response({
			city:resp['name'],
			lat: resp['coord']['lat'],
  			lon: resp['coord']['lon'],
			main:resp['main'],
			weather:resp['weather'],
			visibility:resp['visibility'],
			wind:resp['wind'],
			clouds:resp['clouds']
  		},resp? 200:500,res);
	})
};

function getForecast(req, res){
	let city = geoData['city'];
	if(req['params'] && req['params']['city']){
		city = req['params']['city'];
	}
	getDataAfterByNameCity(city,function(resp){
		let respClient = {
			city:resp['city']['name'],
			lat: resp['city']['coord']['lat'],
  			lon: resp['city']['coord']['lon'],
			list:[]
  		};
  		for(let i in resp['list']){
			respClient['list'].push({
				date: new Date(resp['list'][i]['dt']*1000),
				main:resp['list'][i]['main'],
				weather:resp['list'][i]['weather'],
				visibility:resp['list'][i]['visibility'],
				wind:resp['list'][i]['wind'],
				clouds:resp['list'][i]['clouds']
			});
  		}
		response(respClient,resp? 200:500,res);
	})
};

// Tiempo para una ciudad determinada
function getDataNowByNameCity(city,onComplete){
	let endPoint = hostAPI+'weather?appid='+tokenAPI+'&q='+city.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
	//console.log("getDataNowByNameCity endPoint", endPoint);
	request({
		url: endPoint,
  		method:"GET",
	}, function(error, response, body) {
		if(onComplete){
			if (error) {
				onComplete(false);
			}else{
				onComplete(JSON.parse(body));
			}
		}
	});
};


// Listado del tiempo a una ciudad
function getDataAfterByNameCity(city,onComplete){
	let endPoint = hostAPI+'forecast?appid='+tokenAPI+'&q='+city.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
	//console.log("getDataAfterByNameCity endPoint", endPoint);
	request({
		url: endPoint,
  		method:"GET",
	}, function(error, response, body) {
		if(onComplete){
			if (error) {
				onComplete(false);
			}else{
				onComplete(JSON.parse(body));
			}
		}
	});
};